package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class HouseInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HouseInfoApplication.class, args);
    }

}
