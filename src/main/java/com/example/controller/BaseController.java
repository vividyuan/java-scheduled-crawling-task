package com.example.controller;

import com.example.domain.HouseInfo;
import com.example.service.IHouseInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("houseInfo")
public class BaseController {

    private final IHouseInfoService houseInfoService;

    public BaseController(IHouseInfoService houseInfoService) {
        this.houseInfoService = houseInfoService;
    }

    @GetMapping("/get")
    public HouseInfo getHoseInfo() throws IOException {
        HouseInfo houseInfo = houseInfoService.getHouseInfo();
        Boolean flag = houseInfoService.insertHouseInfo();
        if (flag){
            System.out.println("定时任务启动成功！");
        }else {
            System.out.println("定时任务启动失败！");
        }
        return houseInfo;
    }
}
