package com.example.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.domain.HouseInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IHouseInfoDao extends BaseMapper<HouseInfo> {
    Boolean insertHouseInfo(HouseInfo houseInfo);
}
