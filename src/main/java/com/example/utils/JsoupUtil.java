package com.example.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;


public class JsoupUtil {

    public static Document getHtml(String url, HashMap<String, String> map) throws IOException {
        Document htmlData = Jsoup.connect(url)
                .headers(map)
                .timeout(300000)
                .get();

        return htmlData;
    }
}
