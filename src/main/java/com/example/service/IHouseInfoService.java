package com.example.service;

import com.example.domain.HouseInfo;

import java.io.IOException;

public interface IHouseInfoService {
    HouseInfo getHouseInfo() throws IOException;

    Boolean insertHouseInfo() throws IOException;
}
