package com.example.service.impl;

import com.example.dao.IHouseInfoDao;
import com.example.domain.HouseInfo;
import com.example.domain.house.HouseFirst;
import com.example.domain.house.HouseLast;
import com.example.service.IHouseInfoService;
import com.example.utils.JsoupUtil;
import com.example.utils.Store;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;

@Service
public class HouseInfoServiceImpl implements IHouseInfoService {

    private final IHouseInfoDao houseInfoDao;

    public HouseInfoServiceImpl(IHouseInfoDao houseInfoDao) {
        this.houseInfoDao = houseInfoDao;
    }

    @Override
    public Boolean insertHouseInfo() throws IOException {
        HouseInfo houseInfo = getHouseInfo();
        Boolean flag = houseInfoDao.insertHouseInfo(houseInfo);
        return flag;
    }

    @Override
    public HouseInfo getHouseInfo() throws IOException {
        HouseInfo houseInfo = new HouseInfo();
        String url = "url";
        String url1 = "url";
        HashMap<String, String> map = new HashMap<>();
        map.put("Cookie", Store.cookie);
        Document html1 = JsoupUtil.getHtml(url, map);
        Document html2 = JsoupUtil.getHtml(url1, map);
        HouseFirst houseFirst = getHouseFirst(html1);
        HouseLast houseLast = getHouseLast(html2);
        BeanUtils.copyProperties(houseFirst,houseInfo);
        BeanUtils.copyProperties(houseLast,houseInfo);
        return houseInfo;
    }



    public HouseFirst getHouseFirst(Document body){
        Elements div = body.select("div[id=ctrlPanel]");
        //商品房成交信息
        Element table1 = div.get(0).getElementsByTag("table").get(0);

        String ta = div.select("span[id=ctl03_lblCurTime2]").text();
        String qa = ta.replace("年", "-");
        String ba = qa.replace("月", "-");
        String date = ba.replace("日", "");
        Elements trs1 = table1.getElementsByTag("tr");
        //第二个tr
        Elements ystds1 = trs1.get(1).getElementsByTag("td");
        String yssyts = ystds1.get(1).text();
        String yssymj = ystds1.get(2).text();
        String yskssyts = ystds1.get(3).text();
        String yskssymj = ystds1.get(4).text();
        //第三个tr
        Elements ystds2 = trs1.get(2).getElementsByTag("td");
        String ysbglts = ystds2.get(1).text();
        String ysbglmj = ystds2.get(2).text();
        String ysksbglts = ystds2.get(3).text();
        String ysksbglmj = ystds2.get(4).text();
        //第四个tr
        Elements ystds3 = trs1.get(3).getElementsByTag("td");
        String yszzts = ystds3.get(1).text();
        String yszzmj = ystds3.get(2).text();
        String yskszzts = ystds3.get(3).text();
        String yskszzmj = ystds3.get(4).text();
        //第五个tr
        Elements ystds4 = trs1.get(4).getElementsByTag("td");
        String ysqtts = ystds4.get(1).text();
        String ysqtmj = ystds4.get(2).text();
        String ysksqtts = ystds4.get(3).text();
        String ysksqtmj = ystds4.get(4).text();

        //商品住房按面积统计成交信息
        Element table2 = div.get(0).getElementsByTag("table").get(1);
        Elements trs2 = table2.getElementsByTag("tr");


        System.out.println(trs2.size());

        String ystsunder = "";
        String ysmjunder = "";

        String ystsbt = "";
        String ysmjbt = "";

        String ystsover = "";
        String ysmjover = "";
        for (int i = 1; i < trs2.size(); i++) {
            Elements spzftd = trs2.get(i).getElementsByTag("td");
            System.out.println(spzftd.get(0).text());
            String text = spzftd.get(0).text();
            if(text.equals("90平方米以下")){
                try {
                    ystsunder = spzftd.get(1).text();
                    ysmjunder = spzftd.get(2).text();
                }catch (Exception e){
                    System.out.println("一手商品住房信息不存在");
                }
            }else if(text.equals("90~144平方米")){
                try{
                    ystsbt = spzftd.get(1).text();
                    ysmjbt = spzftd.get(2).text();
                }catch (Exception e){
                    System.out.println("一手商品住房信息不存在");
                }
            }else if(text.equals("144平方米以上")){
                try {
                    ystsover =spzftd.get(1).text();
                    ysmjover = spzftd.get(2).text();
                }catch (Exception e){
                    System.out.println("一手商品住房信息不存在");
                }
            }else if(text.equals("小计")){
                break;
            }

        }

        return HouseFirst.builder().yssyts(yssyts).ysbglts(ysbglts).yszzts(yszzts)
                .ysqtts(ysqtts).yssymj(yssymj).ysbglmj(ysbglmj).yszzmj(yszzmj)
                .ysqtmj(ysqtmj).yskssyts(yskssyts).ysksbglts(ysksbglts).yskszzts(yskszzts)
                .ysksqtts(ysksqtts).yskssymj(yskssymj).ysksbglmj(ysksbglmj).yskszzmj(yskszzmj)
                .ysksqtmj(ysksqtmj).date(date).ystsunder(ystsunder).ysmjunder(ysmjunder).ystsbt(ystsbt)
                .ysmjbt(ysmjbt).ystsover(ystsover).ysmjover(ysmjover).build();
    }

    public HouseLast getHouseLast(Document body){
        Elements div = body.select("div[id=updatepanel1]");
        Element escjxx = div.select("div[class=recordlistBox fix]").get(0);
        Elements escjxxtr = escjxx.getElementsByTag("tr");

        String essymj = "";
        String essyts = "";

        String eszzmj = "";
        String eszzts = "";


        String esqtmj = "";
        String esqtts = "";


        String esbgmj = "";
        String esbgts = "";
        for (int i = 1; i < escjxxtr.size(); i++) {
            Elements estd = escjxxtr.get(i).getElementsByTag("td");
            String text = estd.get(0).text();
            if(text.equals("商业")){

                try {
                    Elements estd1 = escjxxtr.get(1).getElementsByTag("td");
                    essymj = estd1.get(1).text();
                    essyts = estd1.get(2).text();
                }catch (Exception e){
                    System.out.println("二手商业属性不存在");
                }
            }else if(text.equals("住宅")){

                try {
                    Elements estd2 = escjxxtr.get(2).getElementsByTag("td");
                    eszzmj = estd2.get(1).text();
                    eszzts = estd2.get(2).text();
                }catch (Exception e){
                    System.out.println("二手住宅属性不存在");
                }
            }else if(text.equals("其他")){
                try {
                    Elements estd3 = escjxxtr.get(3).getElementsByTag("td");
                    esqtmj = estd3.get(1).text();
                    esqtts = estd3.get(2).text();
                }catch (Exception e){
                    System.out.println("二手其他属性不存在");
                }
            }else if(text.equals("办公")){
                try{
                    Elements estd4 = escjxxtr.get(4).getElementsByTag("td");
                    esbgmj = estd4.get(1).text();
                    esbgts = estd4.get(2).text();
                }catch (Exception e){
                    System.out.println("二手办公属性不存在");
                }
            } else if (text.equals("小计")){
                break;
            }
        }

        return HouseLast.builder().essymj(essymj)
                .essyts(essyts)
                .eszzmj(eszzmj)
                .eszzts(eszzts)
                .esqtmj(esqtmj)
                .esqtts(esqtts)
                .esbgmj(esbgmj)
                .esbgts(esbgts)
                .build();
    }


}
