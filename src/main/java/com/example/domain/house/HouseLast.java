package com.example.domain.house;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HouseLast {
    private String essymj;       //二手面积(㎡)-商业
    private String essyts;       //二手套数-商业

    private String eszzmj;      //二手面积(㎡)-住宅
    private String eszzts;      //二手套数-住宅

    private String esqtmj;       //二手面积(㎡)-其他
    private String esqtts;       //二手套数-其他

    private String esbgmj;       //二手面积(㎡)-办公
    private String esbgts;       //二手套数-办公
}
