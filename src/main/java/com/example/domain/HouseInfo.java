package com.example.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class HouseInfo implements Serializable {

    private String yssyts;       //一手套数-商业
    private String ysbglts;      //一手套数-办公楼
    private String yszzts;       //一手套数-住宅
    private String ysqtts;       //一手套数-其他

    private String yssymj;       //一手面积(㎡)-商业
    private String ysbglmj;       //一手面积(㎡)-办公楼
    private String yszzmj;       //一手面积(㎡)-住宅
    private String ysqtmj;       //一手面积(㎡)-其他

    private String yskssyts;       //一手可售套数-商业
    private String ysksbglts;       //一手可售套数-办公楼
    private String yskszzts;       //一手可售套数-住宅
    private String ysksqtts;       //一手可售套数-其他

    private String yskssymj;       //一手可售面积(㎡)-商业
    private String ysksbglmj;       //一手可售面积(㎡)-办公楼
    private String yskszzmj;       //一手可售面积(㎡)-住宅
    private String ysksqtmj;       //一手可售面积(㎡)-其他

    private String ystsunder;       //一手套数-90平方米以下
    private String ysmjunder;       //一手面积(㎡)-90以下

    private String ystsbt;       //一手套数-90~144平方米
    private String ysmjbt;       //一手面积(㎡)-90~144平方米

    private String ystsover;       //一手套数144平方米以上
    private String ysmjover;       //一手面积(㎡)144平方米以上

    private String essymj;       //二手面积(㎡)-商业
    private String eszzmj;      //二手面积(㎡)-住宅
    private String esqtmj;       //二手面积(㎡)-其他
    private String esbgmj;       //二手面积(㎡)-办公
    private String essyts;       //二手套数-商业
    private String eszzts;      //二手套数-住宅
    private String esqtts;       //二手套数-其他
    private String esbgts;       //二手套数-办公
    private String date;
}
