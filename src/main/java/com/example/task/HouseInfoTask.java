package com.example.task;

import com.example.domain.HouseInfo;
import com.example.service.IHouseInfoService;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@EnableAsync
@EnableScheduling
public class HouseInfoTask {
    private final IHouseInfoService houseInfoService;

    public HouseInfoTask(IHouseInfoService houseInfoService) {
        this.houseInfoService = houseInfoService;
    }

    @Scheduled(cron = "0 0 12 * * ?")
    public void insertHouseInfoTask() throws IOException {
        Boolean flag = houseInfoService.insertHouseInfo();
        if (flag){
            System.out.println("定时任务启动成功！");
        }else {
            System.out.println("定时任务启动失败！");
        }
    }
}
